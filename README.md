# dice_game

This project demonstrates the use of Stateless Widgets in Flutter.
On click, of either of the dices, we print which button is clicked.

It uses the TextButton for the image to be used as a button.

It also overrides the onPressed() method.

The above project is developed with more features using Stateful widgets in branch 'using_stateful_widget'.
Kindly refer for more details.

![dice_app](images/dice.PNG)
